hCs = Simulink.data.dictionary.open('ArDroneDataDictionary.sldd').getSection('Configurations').getEntry('AR_Drone_ConfigSet_NOEXT').getValue;
data = hCs.get_param('CoderTargetData');
IP = data.IP;
droneFtp = ftp([IP ':5551'],'root','root');
mget(droneFtp,'logData.csv');
delete(droneFtp,'logData.csv');
close(droneFtp);
if (exist('logData.mat'))
    delete('logData.mat');
end
logData = csvread('logData.csv');
logData = logData';
dataLength = size(logData,2);
Ts = 1/200; % Hardcoded as of yet
timeVector = 0 : Ts : (dataLength-1) * Ts;
logData = [timeVector; logData];
save('logData.mat','logData');
delete('logData.csv');
