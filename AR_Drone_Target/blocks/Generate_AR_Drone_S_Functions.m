%% Make sure this is done in the correct folder

disp('===================================================================')
disp('               Generating AR Drone Library Blocks                  ')
disp('===================================================================')

originalDir = pwd;
targetDir = which('Generate_AR_Drone_S_Functions');
cd(targetDir(1:end-32));

%% Actuator Initialization

act_init_def = legacy_code('initialize');
act_init_def.SourceFiles = {'act_init.c'};
act_init_def.HeaderFiles = {'act_init.h'};
act_init_def.IncPaths = {''};
act_init_def.SFunctionName = 'Init_Actuator';
act_init_def.Options.language = 'C';
act_init_def.StartFcnSpec = 'Actuator_Initialization(void)';
act_init_def.TerminateFcnSpec = 'Actuator_Stop(void)';

if exist('Init_Actuator.c ','file')~=0
    delete('Init_Actuator.c ')
end

% Generate s function
legacy_code('sfcn_cmex_generate', act_init_def);
legacy_code('compile', act_init_def);
act_init_def.SourceFiles = {'act_init.c','Actuators.c','GPIO.c'};
% Add dependancy for target code generation
act_init_def.SrcPaths = {'.'};
legacy_code('sfcn_tlc_generate', act_init_def);

%% create lib for utilities

lib_name = 'AR_Drone_Utilities';
frontCamBlockName = 'AR_Drone_Front_Camera';
botCamBlockName = 'AR_Drone_Bottom_Camera';
if exist(lib_name, 'file')
	load_system(lib_name);
	set_param(lib_name, 'Lock', 'off')
	try
	delete_block([lib_name, '/Print On Drone']);
	delete_block([lib_name, '/Get Drone Clock']);
	catch
	end
else
	new_system(lib_name, 'Library');
	load_system(lib_name);
end

%% printf

print_def = legacy_code('initialize');
print_def.SourceFiles = {'ARprintf.c'};
print_def.HeaderFiles = {'ARprintf.h'};
print_def.IncPaths = {''};
print_def.SFunctionName = 'ArPrintOnDrone';
print_def.Options.language = 'C';
print_def.StartFcnSpec = 'ARpintf_Init(void)';
print_def.OutputFcnSpec = 'ARpintf_Update(double u1)';
print_def.TerminateFcnSpec = 'ARpintf_Close(void)';

if exist('ARprint.c ','file')~=0
    delete('ARprint.c ')
end

% Generate s function
legacy_code('sfcn_cmex_generate', print_def);
legacy_code('compile', print_def);
lib_def = print_def;

% Add dependancy for target code generation
print_def.SrcPaths = {'.'};
legacy_code('sfcn_tlc_generate', print_def);

%% Clock

clock_def = legacy_code('initialize');
clock_def.SourceFiles = {'ArClock.c'};
clock_def.HeaderFiles = {'ArClock.h'};
clock_def.IncPaths = {''};
clock_def.SFunctionName = 'ArGetDroneClock';
clock_def.Options.language = 'C';
clock_def.StartFcnSpec = 'ArClockInit(void)';
clock_def.OutputFcnSpec = 'ArClockStep(double y1[1])';
clock_def.TerminateFcnSpec = 'ArClockClose(void)';
clock_def.SampleTime    = 'parameterized';

if exist('ARclock_Sfcn.c ','file')~=0
    delete('ARclock_Sfcn.c ')
end

% Generate s function
legacy_code('sfcn_cmex_generate', clock_def);
legacy_code('compile', clock_def);
lib_def = [lib_def clock_def];

% Add dependancy for target code generation
clock_def.SrcPaths = {'.'};
legacy_code('sfcn_tlc_generate', clock_def);

%% create blocks lib

legacy_code('slblock_generate',lib_def,lib_name);
set_param(lib_name, 'EnableLBRepository','on');
set_param(lib_name, 'Lock', 'on');
save_system(lib_name);
bdclose;

%% IMU Block

% load the IMU Packet bus
load('IMU_Packets_Bus.mat')

IMU_Block_def = legacy_code('initialize');
IMU_Block_def.SFunctionName = 'IMU_Sfcn_mex';
IMU_Block_def.InitializeConditionsFcnSpec  = 'void MDL_IMU_start()';
IMU_Block_def.TerminateFcnSpec = 'void MDL_IMU_term()';
IMU_Block_def.OutputFcnSpec                = 'void MDL_IMU_step(IMU_Packets y1[1], int32 y2[1], int32 y3[1], int32 u1)';  
IMU_Block_def.SourceFiles                  = {'IMU_Navdata_Wrapper.c'};
IMU_Block_def.IncPaths                     = {''};
IMU_Block_def.Options.useTlcWithAccel      = false;
IMU_Block_def.Options.language =  'C';
IMU_Block_def.SampleTime    = 'parameterized';

if exist('IMU_Sfcn_mex.c ','file')~=0
    delete('IMU_Sfcn_mex.c ')
end

% Generate S-function source file
legacy_code('sfcn_cmex_generate', IMU_Block_def);

% Generate for simulation only - the S-function does nothing in normal mode
legacy_code('generate_for_sim', IMU_Block_def);

% When generating TLC, we want to include additional source files which are
% device driver / HW specific
IMU_Block_def.SourceFiles  = {'IMU_Navdata.c','IMU_Navdata_wrapper.c'};
IMU_Block_def.HeaderFiles  = {'IMU_Navdata.h'};
IMU_Block_def.SrcPaths = {'.'};
legacy_code('sfcn_tlc_generate', IMU_Block_def);

%% LED Block

LED_Block_def = legacy_code('initialize');
LED_Block_def.SourceFiles = {'led.c'};
LED_Block_def.HeaderFiles = {'led.h'};
LED_Block_def.IncPaths = {''};
LED_Block_def.SFunctionName = 'ARDrone_LED';
LED_Block_def.Options.language = 'C';
LED_Block_def.OutputFcnSpec = 'LED_set(uint8 u1[4])';

if exist('ARDrone_LED.c ','file')~=0
    delete('ARDrone_LED.c ')
end

% Generate s function
legacy_code('sfcn_cmex_generate', LED_Block_def);
legacy_code('compile', LED_Block_def);

% Add dependancy for target code generation
LED_Block_def.SourceFiles = {'led.c','Actuators.c','GPIO.c'};
LED_Block_def.SrcPaths = {'.'};
legacy_code('sfcn_tlc_generate', LED_Block_def);

%% Motor Block

Motor_def = legacy_code('initialize');
Motor_def.SourceFiles = {'motor.c'};
Motor_def.HeaderFiles = {'motor.h'};
Motor_def.IncPaths = {''};
Motor_def.SFunctionName = 'ARDrone_Motor';
Motor_def.Options.language = 'C';
Motor_def.OutputFcnSpec = 'uint16 y1 = Motor_Set(uint8 u1[5])';

if exist('ARDrone_Motor.c ','file')~=0
    delete('ARDrone_Motor.c ')
end

% Generate s function
legacy_code('sfcn_cmex_generate', Motor_def);
legacy_code('compile', Motor_def);

% Add dependancy for target code generation
Motor_def.SourceFiles = {'motor.c','Actuators.c','GPIO.c'};
legacy_code('sfcn_tlc_generate', Motor_def);

%% Version Check Block
Version_Check_def = legacy_code('initialize');
Version_Check_def.SourceFiles = {'versionCheck.c'};
Version_Check_def.HeaderFiles = {'versionCheck.h'};
Version_Check_def.IncPaths = {''};
Version_Check_def.SFunctionName = 'Version_Check';
Version_Check_def.Options.language = 'C';
Version_Check_def.StartFcnSpec = 'void versionCheckInit(void)';
Version_Check_def.OutputFcnSpec = 'void versionCheckStep(uint8 y1[1])';
Version_Check_def.TerminateFcnSpec = 'void versionCheckClose(void)';

if exist('Version_Check.c ','file')~=0
    delete('Version_Check.c ')
end

% Generate s function
legacy_code('sfcn_cmex_generate', Version_Check_def);
legacy_code('compile', Version_Check_def);
Version_Check_def.SrcPaths = {'.'};
legacy_code('sfcn_tlc_generate', Version_Check_def);

%% Battery block
Battery_def = legacy_code('initialize');
Battery_def.SFunctionName = 'Battery_Sfcn_mex';
Battery_def.InitializeConditionsFcnSpec  = 'void BatteryMeasure_start()';
Battery_def.TerminateFcnSpec = 'void BatteryMeasure_term()';
Battery_def.OutputFcnSpec                = 'void BatteryMeasure_step(single y1[1])';  
Battery_def.SourceFiles                  = {'BatteryMeasure_Wrapper.c'};
Battery_def.IncPaths                     = {''};
Battery_def.Options.useTlcWithAccel      = false;
Battery_def.Options.language =  'C';
Battery_def.SampleTime    = 'parameterized';

if exist('Battery_Sfcn_mex.c ','file')~=0
    delete('Battery_Sfcn_mex.c ')
end

% Generate S-function source file
legacy_code('sfcn_cmex_generate', Battery_def);

% Generate for simulation only - the S-function does nothing in normal mode
legacy_code('generate_for_sim', Battery_def);

% When generating TLC, we want to include additional source files which are
% device driver / HW specific
Battery_def.HeaderFiles                  = {'i2c-dev.h'};
Battery_def.SourceFiles  = {'BatteryMeasure.c','BatteryMeasure_Wrapper.c'};
Battery_def.SrcPaths = {'.'};
legacy_code('sfcn_tlc_generate', Battery_def);

%% Generate the rtwmakecfg file for all s functions

legacy_code('rtwmakecfg_generate', [print_def; act_init_def;IMU_Block_def;LED_Block_def;Motor_def;Version_Check_def;Battery_def; clock_def]);

%% clear the loaded bus object

clear('IMU_Packets');

%% return to original path
cd(originalDir);